﻿using Raven.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DocumentStorage.Interface
{
    public interface IDocumentStoreSeed
    {
        void SeedDocumentStore(IDocumentStore DocumentStorage);
    }
}
