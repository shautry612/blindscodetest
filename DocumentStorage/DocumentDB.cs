﻿using DocumentStorage.Interface;
using Raven.Client.Embedded;
using Raven.Client.UniqueConstraints;

namespace DocumentStorage
{
    public class DocumentDB : EmbeddableDocumentStore
    {
        public DocumentDB()
        {
            this.DefaultDatabase = "BlindsTestDB";
            this.DataDirectory = "C:/temp/Electronic";
            //this.Configuration.Storage.Voron.AllowOn32Bits = true;
            //this.Configuration.AllowLocalAccessWithoutAuthorization = true;
            this.UseEmbeddedHttpServer = true;

            this.RegisterListener(new UniqueConstraintsStoreListener());
            this.Initialize();
        }

        public DocumentDB( IDocumentStoreSeed DocumentLoader )
            : this()
        {
            SeedDB(DocumentLoader);
        }

        public void SeedDB( IDocumentStoreSeed DocumentLoader )
        {
            DocumentLoader.SeedDocumentStore(this);
        }
    }
}