﻿using Raven.Client.UniqueConstraints;

namespace DocumentStorage
{
    public class ResistorColorCode
    {
        [UniqueConstraint]
        public string Color { get; set; }
        public int? SignificantDigit { get; set; }
        public double? Multiplier { get; set; }
        public double? Tolerance { get; set; }
        public char? ToleranceCode { get; set; }
    }
}