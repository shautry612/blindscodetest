using DataLayer.Util;
using ElectricComponentCalculator.Utilities;
using Microsoft.Practices.Unity;
using Raven.Client;
using System;
using System.Web.Http;
using System.Web.Mvc;
using Unity.WebApi;

namespace ElectricComponentCalculator
{
    public static class UnityConfig
    {
        private static readonly Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        public static void RegisterComponents()
        {
            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            DependencyResolver.SetResolver(new Microsoft.Practices.Unity.Mvc.UnityDependencyResolver(container.Value));
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container.Value);
        }

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        /// 
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }

        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterInstance<IDocumentStore>(DataStoreFactory.GetDocumentDB(new DocumentStoreInit()));
        }
    }
}