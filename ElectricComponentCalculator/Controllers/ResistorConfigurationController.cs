﻿using DocumentStorage;
using Raven.Client;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace ElectricComponentCalculator.Controllers
{
    public class ResistorConfigurationController : Controller
    {
        private readonly IDocumentStore db;

        public ResistorConfigurationController(IDocumentStore db)
        {
            this.db = db;
        }

        // GET: ResistorConfiguration
        public ActionResult Index()
        {
            IList<ResistorColorCode> model = new List<ResistorColorCode>();

            using (var session = db.OpenSession())
            {
                model = (from r in session.Query<ResistorColorCode>()
                               select r).ToList();
            }
            
            return View(model);
        }

        [HttpPost]
        public ActionResult GetItem(string color)
        {
            ResistorColorCode code = null;

            using (var session = db.OpenSession())
            {
                code = session.Query<ResistorColorCode>().FirstOrDefault(r => r.Color == color);

                if ( code == null )
                {
                    return new HttpNotFoundResult();
                }
            }

            return PartialView("_ItemResistorCode", code);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View("CreateResistorColor");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( ResistorColorCode colorCode )
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // verify does not already exist
            using (var session = db.OpenSession())
            {
                var exists = session.Query<ResistorColorCode>().Any(r => r.Color == colorCode.Color);

                if ( exists )
                {
                    ModelState.SetModelValue("Color", new ValueProviderResult("Color already exists", colorCode.Color, CultureInfo.InvariantCulture));
                    return View();
                }

                session.Store(colorCode);
                session.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateResistorColor( string Color)
        {
            ResistorColorCode code = null;

            using (var session = db.OpenSession())
            {
                code = session.Query<ResistorColorCode>().FirstOrDefault(r => r.Color == Color);
                if ( code == null )
                {
                    return new HttpNotFoundResult();
                }
            }

            return PartialView("_EditResistorCode", code);
        }

        [HttpPost]
        public ActionResult Save( ResistorColorCode model )
        {
            if (!ModelState.IsValid )
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            using (var session = db.OpenSession())
            {
                session.Store(model);
                session.SaveChanges();
            }

            return PartialView("_ItemResistorCode", model);
        }


        [AcceptVerbs(HttpVerbs.Post | HttpVerbs.Delete)]
        public ActionResult DeleteResistorColor(string color)
        {
            using (var session = db.OpenSession())
            {
                var code = session.Query<ResistorColorCode>().FirstOrDefault(r => r.Color == color);

                if ( code == null )
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Color does not exist!");
                }

                session.Delete(code);
                session.SaveChanges();
            }

            return RedirectToAction("Index");
        }

        //[HttpGet]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteResistorColor(ResistorColorCode item)
        //{
        //    using (var session = db.OpenSession())
        //    {
        //        var code = session.Query<ResistorColorCode>().FirstOrDefault(r => r.Color == item.Color);

        //        if (code == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Color does not exist!");
        //        }

        //        session.Delete(code);
        //        session.SaveChanges();
        //    }

        //    return new HttpStatusCodeResult(HttpStatusCode.OK);
        //}
    }
}