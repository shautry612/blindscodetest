﻿using DataLayer;
using Raven.Client;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ElectricComponentCalculator.Api
{
    public class ResistanceCalculatorController : ApiController
    {
        private IDocumentStore db = null;

        public ResistanceCalculatorController(IDocumentStore store)
            : base()
        {
            db = store;
        }

        [ResponseType(typeof(int))]
        [Route("api/ResistanceCalculator/CalcOhms")]
        public async Task<IHttpActionResult> GetOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor)
        {
            if ( !ModelState.IsValid )
            {
                return BadRequest();
            }

            int ohm = 0;

            Resistor calc = new Resistor(db);

            ohm = await Task<int>.Run(() => { return calc.CalculateOhmValue(bandAColor, bandBColor, bandCColor, bandDColor); });

            return Ok(ohm);
        }
    }
}