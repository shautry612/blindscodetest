﻿using DocumentStorage;
using Raven.Client;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ElectricComponentCalculator.Api
{
    public class ResistorCodeController : ApiController
    {
        private readonly IDocumentStore db;

        public ResistorCodeController( IDocumentStore db )
        {
            this.db = db;
        }
        // GET api/<controller>
        [Route("api/ResistorCode/GetAll")]
        public IEnumerable<ResistorColorCode> GetAll()
        {
            IEnumerable<ResistorColorCode> codes = new List<ResistorColorCode>();

            using (var session = db.OpenSession())
            {
                codes = session.Query<ResistorColorCode>().ToList();
            }

            return codes;
        }

        // GET api/<controller>/5
        [Route("api/ResistorCode/Get/Color")]
        public ResistorColorCode Get(string Color)
        {
            ResistorColorCode code = null;

            using (var session = db.OpenSession())
            {
                code = session.Query<ResistorColorCode>().FirstOrDefault(c => c.Color == Color);
            }

            return code;
        }

        // POST api/<controller>
        [Route("api/ResistorCode/Update")]
        public void Post([FromBody] ResistorColorCode ColorCode)
        {
        }

        // PUT api/<controller>/5

        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/ResistorCode/Delete/green
        [AcceptVerbs("DELETE", "POST")]
        [Route("api/ResistorCode/Delete")]
        public IHttpActionResult Delete(string color)
        {
            using (var session = db.OpenSession())
            {
                var code = session.Query<ResistorColorCode>().FirstOrDefault(r => r.Color == color);

                if (code == null)
                {
                    return BadRequest("Color does not exist!");
                }

                session.Delete(code);
                session.SaveChanges();
            }
            return Ok(color);
        }
    }
}