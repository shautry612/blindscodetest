﻿using DocumentStorage;
using DocumentStorage.Interface;

namespace ElectricComponentCalculator.Utilities
{
    public sealed class DataStoreFactory
    {
        public static readonly DocumentDB db = new DocumentDB();

        public static DocumentDB GetDocumentDB()
        {
            return db;
        }

        public static DocumentDB GetDocumentDB( IDocumentStoreSeed docSeed )
        {
            db.SeedDB(docSeed);
            return db;
        }
    }
}