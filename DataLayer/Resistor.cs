﻿using DocumentStorage;
using ElectricComponentInterfaces;
using Raven.Client;
using Raven.Client.Linq;
using System;
using System.Linq;

namespace DataLayer
{
    public class Resistor : IOhmValueCalculator
    {
        private IDocumentStore _db;
        public Resistor(IDocumentStore db)
        {
            _db = db;
        }

        public int CalculateOhmValue(string bandAColor, string bandBColor, string bandCColor, string bandDColor = "None")
        {
            ResistorColorCode[] resistorCodes = null;
            int ohms = 0;

            //Check for at least bandAColor
            if ( string.IsNullOrWhiteSpace(bandAColor) )
            {
                throw new ArgumentException(String.Format("Invalid value for bandAColor - '{bandAColor}'."));
            }

            // Pull color codes from database in same order as the color bands
            string[] colorBands = (new string[] { bandAColor, bandBColor, bandCColor, bandDColor, bandDColor }).Where(c => !string.IsNullOrWhiteSpace(c)).ToArray();

            using (var session = _db.OpenSession())
            {
                resistorCodes = (from args in colorBands 
                                join code in session.Query<ResistorColorCode>() on args equals code.Color
                                select code).ToArray();
            }

            //There must be at least a valid color code for bandAcolor
            if ( resistorCodes.Length == 0 || !resistorCodes[0].SignificantDigit.HasValue )
            {
                throw new ArgumentException("Specified color codes are not valid.");
            }

            // calculate ohms with using tolerance codes
            for( int i = 0; i < Math.Min( 3, resistorCodes.Length); i++ )
            {
                switch(i)
                {
                    case 0:
                    case 1:
                        if (resistorCodes[i].SignificantDigit.HasValue)
                        {
                            ohms = (ohms * i * 10) + resistorCodes[i].SignificantDigit.Value;
                        }
                        break;

                    case 2:
                        if (resistorCodes[i].Multiplier.HasValue)
                        {
                            ohms *= (int)resistorCodes[i].Multiplier.Value;
                        }
                        break;

                    default:
                        break;             
                }
            }

            return ohms;
        }
    }
}
