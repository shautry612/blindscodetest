﻿using DocumentStorage;
using DocumentStorage.Interface;
using Raven.Abstractions.Data;
using Raven.Client;
using Raven.Client.Connection;
using Raven.Client.Indexes;
using System;
using System.Linq;

namespace DataLayer.Util
{
    public class DocumentStoreInit : IDocumentStoreSeed
    {
        public void SeedDocumentStore(IDocumentStore DocumentStorage)
        {
            //DeleteAllDocs(DocumentStorage, "ResistorColorCodes");

            IndexDefinitionBuilder<ResistorColorCode> builder = new IndexDefinitionBuilder<ResistorColorCode>();
            builder.Map = (codes) => from code in codes
                                      select new { Color = code.Color };

            //DocumentStorage.DatabaseCommands.PutIndex("ResistorColorCode/Color", builder.ToIndexDefinition(DocumentStorage.Conventions));

            ResistorColorCode[] resistorColorCodes = new ResistorColorCode[] {
               new ResistorColorCode() { Color = "Black",  SignificantDigit = 0,    Multiplier = Math.Pow(10.0, 0.0),  Tolerance = null, ToleranceCode = null },
               new ResistorColorCode() { Color = "Brown",  SignificantDigit = 1,    Multiplier = Math.Pow(10.0, 1.0),  Tolerance = 1,    ToleranceCode = 'F' },
               new ResistorColorCode() { Color = "Red",    SignificantDigit = 2,    Multiplier = Math.Pow(10.0, 2.0),  Tolerance = 2,    ToleranceCode = 'G' },
               new ResistorColorCode() { Color = "Orange", SignificantDigit = 3,    Multiplier = Math.Pow(10.0, 3.0),  Tolerance = null, ToleranceCode = null },
               new ResistorColorCode() { Color = "Yellow", SignificantDigit = 4,    Multiplier = Math.Pow(10.0, 4.0),  Tolerance = 5,    ToleranceCode = null },
               new ResistorColorCode() { Color = "Green",  SignificantDigit = 5,    Multiplier = Math.Pow(10.0, 5.0),  Tolerance = .5,   ToleranceCode = 'D' },
               new ResistorColorCode() { Color = "Blue",   SignificantDigit = 6,    Multiplier = Math.Pow(10.0, 6.0),  Tolerance = .25,  ToleranceCode = 'C' },
               new ResistorColorCode() { Color = "Violet", SignificantDigit = 7,    Multiplier = Math.Pow(10.0, 7.0),  Tolerance = .1,   ToleranceCode = 'B' },
               new ResistorColorCode() { Color = "Gray",   SignificantDigit = 8,    Multiplier = Math.Pow(10.0, 8.0),  Tolerance = 10,   ToleranceCode = 'A' },
               new ResistorColorCode() { Color = "White",  SignificantDigit = 9,    Multiplier = Math.Pow(10.0, 9.0),  Tolerance = null, ToleranceCode = null },
               new ResistorColorCode() { Color = "Gold",   SignificantDigit = null, Multiplier = Math.Pow(10.0, -1.0), Tolerance = 5,    ToleranceCode = 'J' },
               new ResistorColorCode() { Color = "Silver", SignificantDigit = null, Multiplier = Math.Pow(10.0, -2.0), Tolerance = 10,   ToleranceCode = 'K' },
               new ResistorColorCode() { Color = "None",   SignificantDigit = null, Multiplier = null,                 Tolerance = 20,   ToleranceCode = 'M' }
            };

            using (var bulkInsert = DocumentStorage.BulkInsert(options: new BulkInsertOptions() { OverwriteExisting = true }))
            {
                foreach (ResistorColorCode code in resistorColorCodes)
                {
                    bulkInsert.Store(code, code.Color);
                }
            }
        }

        private void DeleteAllDocs(IDocumentStore documentStorage, String collectionName)
        {
            Operation operation = documentStorage.DatabaseCommands.DeleteByIndex("Raven/DocumentsByEntityName", new IndexQuery() { Query = "Tag:" + collectionName }, new BulkOperationOptions() { AllowStale = true });

            operation.WaitForCompletion();
        }
    }
}