﻿using DataLayer;
using DataLayer.Util;
using DocumentStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Raven.Client;
using System.Linq;

namespace TestDataLayer
{
    [TestClass]
    public class UnitTest1
    {
        private IDocumentStore db;

        [TestInitialize]
        public void testInit()
        {
            db = new DocumentDB(new DocumentStoreInit());
        }

        [TestCleanup]
        public void testCleanup()
        {
            db.Dispose();
        }

        [TestMethod]
        public void TestDBSeeding()
        {
            using (var session = db.OpenSession())
            {
                Assert.AreEqual(13, session.Query<ResistorColorCode>().Count());
            }
        }

        [TestMethod]
        public void TestOneBlackBand()
        {
            var calculator = new Resistor(db);

            Assert.AreEqual(0, calculator.CalculateOhmValue("Black", "None", "None", "None"));
        }

        [TestMethod]
        public void TestYellowVioletRedGold()
        {
            var calculator = new Resistor(db);

            Assert.AreEqual(4700, calculator.CalculateOhmValue("Yellow", "Violet", "Red", "Gold"));
        }
    }
}